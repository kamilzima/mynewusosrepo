﻿using System;
using System.Collections.Generic;
using MyOwnUsos.Models;

namespace MyOwnUsos.Others
{
    public static class GeneratingDbObjectsClass
    {
        private static List<string> _name = new List<string> { "Jan", "Kuba", "Marcin" };

        private static List<string> _surname = new List<string> { "Chudzikiewicz", "Chudy", "Brzoza" };
        private static Array _academicDegrees = Enum.GetValues(typeof(Enums.ScienceDegrees));
        private static Random _randomGenerator = new Random();
        public static IEnumerable<Teacher> GeneratePersons(int count)
        {
                var listOfPersons = new List<Teacher>();
                for (int i = 0; i < count; i++)
                {
                    var randomNumberForNames = _randomGenerator.Next(0, _name.Count);
                    var randomNumberForSurnames = _randomGenerator.Next(0, _surname.Count);
                    var randomNumberForDegress = _randomGenerator.Next(0, _academicDegrees.Length);
                    listOfPersons.Add(new Teacher()
                    {
                        FirstName = _name[randomNumberForNames],
                        LastName = _surname[randomNumberForSurnames],
                        AcademicDegree = (Enums.ScienceDegrees)_academicDegrees.GetValue(randomNumberForDegress)
                    });
                }
                return listOfPersons;
        }
    }
}
