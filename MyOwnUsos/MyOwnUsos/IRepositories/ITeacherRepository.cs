﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyOwnUsos.Models;

namespace MyOwnUsos.IRepositories
{
    public interface ITeacherRepository
    {
        IEnumerable<Teacher> Teachers { get; }
        Teacher GetTeacher(int teacherId);
    }
}
