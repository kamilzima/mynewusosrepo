﻿using System.Collections.Generic;

namespace MyOwnUsos.Models
{
    public class Teacher:Person 
    {
        public int TeacherId { get; set; }
        public List<Lesson> Lessons { get; set; }
    }
}