﻿using System.Collections.Generic;

namespace MyOwnUsos.Models
{
    public class StudentsGroup
    {
        public int StudentsGroupId { get; set; }
        public string GroupName { get; set; }
        public string Info { get; set; }
        public virtual List<LessonsSet> LessonsSets{ get; set; }
        public virtual List<Student> Students { get; set; }
    }
}