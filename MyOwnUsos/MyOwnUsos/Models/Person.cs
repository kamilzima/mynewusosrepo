﻿using System;

namespace MyOwnUsos.Models
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAdress { get; set; }
        public Enums.ScienceDegrees AcademicDegree { get; set; }
        public string FullName
        {
            get { return String.Concat(FirstName, " ", LastName); }
        }
        public string FullNameWithDegree
        {
            get { return String.Concat(AcademicDegree," ",FirstName, " ", LastName); }
        }
    }
}
