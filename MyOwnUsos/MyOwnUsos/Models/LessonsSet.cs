﻿using System.Collections.Generic;

namespace MyOwnUsos.Models
{
    public class LessonsSet
    {
        public int LessonsSetId { get; set; }
        public string Description { get; set; }
        public virtual List<Lesson> Lessons { get; set; }
        public virtual List<StudentsGroup> StudentsGroups { get; set; }
    }
}