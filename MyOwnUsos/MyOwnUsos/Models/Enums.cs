﻿namespace MyOwnUsos.Models
{
    public class Enums
    {
        public enum Mark {
            Dopuszczajacy,
            DostatecznyMinus, Dostateczny, DostatecznyPlus,
            DobryMinus, Dobry, DobryPlus,
            BardzoDobryMinus, BardzoDobry, BardzoBardzoDobryPlus,
        }

        public enum ScienceDegrees
        {
            Licencjat,
            Inzynier,
            Magister,
            MagisterInzynier,
            Doktor,
            DoktorInzynier,
            ProfesorUczelniany,
            ProfesorZwyczajny
        }
    }
}
