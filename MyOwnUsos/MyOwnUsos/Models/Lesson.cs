﻿namespace MyOwnUsos.Models
{
    public class Lesson
    {
        public int LessonId { get; set; }
        public string Desciprion { get; set; }
        public  Enums.Mark Mark { get; set; }
        public virtual LessonsSet LessonsSet { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}