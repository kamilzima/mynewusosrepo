﻿using System;

namespace MyOwnUsos.Models
{
    public class Student:Person
    {
        public int StudentId { get; set; }
        public virtual StudentsGroup StudentsGroup { get; set; }
    }
}
